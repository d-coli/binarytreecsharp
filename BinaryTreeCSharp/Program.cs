﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinaryTreeCSharp
{
    public class CheckIfBalancedBinaryTree
    {

        class TreeNode
        {
            public TreeNode left;
            public TreeNode right;
            public int value;

            public TreeNode(int value)
            {
                this.value = value;
            }
        }

        TreeNode root;

        /*
                                0
                          1             2
                      3      4       5  
                        6              7
                          8
        */

        //returns false
        private TreeNode FalseTree()
        {
            this.root = new TreeNode(0);
            TreeNode n1 = new TreeNode(1);
            TreeNode n2 = new TreeNode(2);
            TreeNode n3 = new TreeNode(3);
            TreeNode n4 = new TreeNode(4);
            TreeNode n5 = new TreeNode(5);
            TreeNode n6 = new TreeNode(6);
            TreeNode n7 = new TreeNode(7);
            TreeNode n8 = new TreeNode(8);

            root.left = n1;
            root.right = n2;

            n1.left = n3;
            n1.right = n4;

            n2.left = n5;

            n3.right = n6;

            n5.right = n7;

            n6.right = n8;

            return root;
        }

        //      1
        //  2       3

        //returns True
        private TreeNode TrueTree()
        {
            this.root = new TreeNode(1);
            TreeNode n1 = new TreeNode(2);
            TreeNode n2 = new TreeNode(3);

            root.left = n1;
            root.right = n2;

            return root;
        }


        // this function returns height of tree rooted at currentNode if the tree is balanced
        // otherwise it returns -1
        private int CheckBalance(TreeNode currentNode)
        {
            if (currentNode == null)
            {
                return 0;
            }

            // check if left sub-tree is balanced
            int leftSubtreeHeight = CheckBalance(currentNode.left);
            if (leftSubtreeHeight == -1) return -1;

            // check if right sub-tree is balanced
            int rightSubtreeHeight = CheckBalance(currentNode.right);
            if (rightSubtreeHeight == -1) return -1;

            // if both sub-trees are balanced, check the difference of heights
            // should be less than or equal to 1 
            if (Math.Abs(leftSubtreeHeight - rightSubtreeHeight) > 1)
            {
                return -1;
            }

            // if tree rooted at this node is balanced, return height if tree rooted at this this node
            return (Math.Max(leftSubtreeHeight, rightSubtreeHeight) + 1);
        }

        public bool CheckIfBalanced()
        {
            Thread.Sleep(5000); // mimic work being done
            if (CheckBalance(root) == -1)
            {
                return false;
            }
            return true;
        }

        public class TreeTask
        {
            public bool FinishStatus { get; set; }
            public string Result { get; set; }

            public TreeTask(bool finishStatus, string result)
            {
                this.FinishStatus = finishStatus;
                this.Result = result;

            }
        }

        //Coordinator class
        public class Coordinator
        {
            public async Task<bool> AddTask(TreeTask taskObj)
            {
                CheckIfBalancedBinaryTree tree = new CheckIfBalancedBinaryTree();
                Console.WriteLine("Which tree? Enter 1 for a balanced tree. Enter 0 for an unbalanced tree");
                string userOption = Console.ReadLine();

                if (userOption == "1")
                {
                    tree.TrueTree();
                }
                else if (userOption == "0")
                {
                    tree.FalseTree();
                }

                else
                {
                    Console.WriteLine("");
                    Console.WriteLine("ERROR: " + userOption + " is not an option. Bye :<");
                    Console.ReadKey();
                    System.Environment.Exit(1);
                }

                bool yieldResult = await Task.Run(() => tree.CheckIfBalanced());

                Console.WriteLine("Is the tree balanced?: " + yieldResult);
                taskObj.Result = yieldResult.ToString();
                TaskComplete(taskObj);
                TaskShouldYield(taskObj);

                Console.WriteLine("Thank you for your time...press another key to exit this application.");
                               
                return true;
            }

            public void TaskComplete(TreeTask taskObj)
            {
                taskObj.FinishStatus = true;
            }

            public bool TaskShouldYield(TreeTask taskObj)
            {
                if (taskObj.FinishStatus == true)
                {
                    Console.WriteLine("-- Task Complete --");
                    Console.WriteLine("Task result: " + taskObj.Result);
                    return true;
                }
                else
                {
                    return false;
                }


            }
        }

        // Main
        public static void Main(string[] args)
        {
            CheckIfBalancedBinaryTree tree = new CheckIfBalancedBinaryTree();

            TreeTask t = new TreeTask(false, "empty");
            Coordinator c = new Coordinator();

            Console.WriteLine("Welcome to my Binary Tree Solution...");
            Console.WriteLine("");

            Console.WriteLine("Press any key to continue");
            Console.WriteLine("");

            Console.ReadKey(true);

            var TaskResult = c.AddTask(t);

            Console.WriteLine("Processing...");
            Console.ReadKey();
        }
    }
}
